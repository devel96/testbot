<?php


namespace app\commands;

require_once ROOT . '/classes/commands/Command.php';
require_once ROOT . '/classes/models/History.php';

use app\models\History;

/**
 * Class HistoryCommand
 * @package app\commands
 */
class HistoryCommand extends Command
{
    /**
     * @return array
     */
    public function getData(): array
    {
        $history = new History();
        $data = $history->setUserId($this->request->getUserId())->getAll();

        return [
            'text' => 'Здраствуйте ' . $this->request->getFirstName() .
                ". Ваши последние 30 запросы (максимум)  >>>>>>>> " .
                $this->makeText($data),
            'reply_markup' => ['hide_keyboard' => true]
        ];
    }

    /**
     * @param array $data
     * @return string
     */
    private function makeText(array $data): string
    {
        $text = '';

        foreach ($data as $history) {
            $text .= date('Y-m-d H:i:s', $history['date']) . '[' . $history['value'] . '] >>>> ';
        }

        return $text;
    }

}