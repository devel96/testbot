<?php


namespace app\models;

require_once ROOT . '/classes/db/DB.php';

/**
 * Class Model
 * @package app\models
 */
abstract class Model
{
    protected \DB $_db;

    public function __construct()
    {
        //Создаем композицию
        $this->_db = new \DB();
    }
}