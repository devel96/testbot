<?php

namespace app\controllers;

require_once ROOT . '/classes/models/TelegramRequestObject.php';
require_once ROOT . '/classes/facades/Log.php';
require_once ROOT . '/classes/commands/Command.php';
require_once ROOT . '/classes/commands/FollowCommand.php';
require_once ROOT . '/classes/commands/CheckDollarCommand.php';
require_once ROOT . '/classes/commands/HistoryCommand.php';

use app\commands\HistoryCommand;
use app\models\TelegramRequestObject;
use app\facades\Log;
use app\commands\Command;
use app\commands\FollowCommand;
use app\commands\CheckDollarCommand;

/**
 * Class TelegramBot
 */
class TelegramBot
{
    public const CURL_URL = 'https://api.telegram.org/bot1126136628:AAFvtXWcmdtbDsmilHyaEQvNiw5rfEY8n04/';

    private TelegramRequestObject $request;
    private string $method = 'sendMessage';
    private array $data = [];
    private array $headers = [];


    /**
     * @param TelegramRequestObject $request
     */
    public function setRequest(TelegramRequestObject $request): void
    {
        $this->request = $request;
    }

    /**
     * TelegramBot constructor.
     * @param TelegramRequestObject $request
     */
    public function __construct(TelegramRequestObject $request)
    {
        $this->setRequest($request);

        if (!$this->request->getMessageId()) {
            Log::error([
                'type' => Log::TELEGRAM_REQUEST_ERROR,
                'message' => 'Invalid Request From Telegram',
                'date' => date('Y-m-d H:i:s')
            ]);
        }
    }

    /**
     * Run bot functionality
     */
    public function run()
    {
        switch ($this->request->getCommand()) {
            case Command::FOLLOW :
                $command = new FollowCommand($this->request);
                $this->data = $command->getData();
                $this->headers = $command->getHeaders();

                Log::success([
                    'command' => Command::FOLLOW,
                    'message' => $this->request,
                    'date' => date('Y-m-d H:i:s')
                ]);

                break;

            case Command::CHECK_DOLLAR_COURSE:
                $command = new CheckDollarCommand($this->request);
                $this->data = $command->getData();
                $this->headers = $command->getHeaders();

                Log::success([
                    'command' => Command::CHECK_DOLLAR_COURSE,
                    'message' => $this->request,
                    'date' => date('Y-m-d H:i:s')
                ]);

                break;

            case Command::HISTORY:
                $command = new HistoryCommand($this->request);
                $this->data = $command->getData();
                $this->headers = $command->getHeaders();

                Log::success([
                    'command' => Command::HISTORY,
                    'message' => $this->request,
                    'date' => date('Y-m-d H:i:s')
                ]);

                break;
            default:
                $command = new Command();
                $this->data = $command->getData();
                $this->headers = $command->getHeaders();

                Log::success([
                    'command' => Log::TELEGRAM_REQUEST_SUCCESS,
                    'message' => $this->request,
                    'date' => date('Y-m-d H:i:s')
                ]);

                break;
        }

        $this->data['chat_id'] = $this->request->getUserId();

        $this->send();
    }

    /**
     * Send a curl request to telegram
     */
    private function send()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => self::CURL_URL . $this->method,
            CURLOPT_POSTFIELDS => json_encode($this->data),
            CURLOPT_HTTPHEADER => array_merge(array("Content-Type: application/json"), $this->headers)
        ]);

        curl_exec($curl);

        if ($errorMessage = curl_errno($curl)) {
            Log::error([
                'type' => Log::CURL_SENDING_ERROR,
                'message' => $errorMessage,
                'date' => date('Y-m-d H:i:s')
            ]);
        }
        curl_close($curl);
    }
}